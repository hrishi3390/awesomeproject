#include "Humanoid.h"
#include "Global.h"

GLfloat shoulder = 0; //go to 20 t0 -20
static GLfloat elbow = 45; // 
static GLfloat knee = 0;
static GLfloat thung = 0;
GLUquadric *quadric = NULL;

GLfloat rightShoulder = 0.0f;
GLfloat rightElbow = 0.0f;
GLfloat leftShoulder = 0.0f;
GLfloat leftElbow = 0.0f;

GLfloat rightThung = 0.0f;
GLfloat rightKnee = 0.0f;
GLfloat leftThung = 0.0f;
GLfloat leftKnee = 0.0f;


void updateShoulder(GLfloat speed, GLfloat angle)
{
	static bool leftBoudary = false;
	static bool rightBoundary = true;

	if (leftBoudary)
	{
		
		if (shoulder >= angle) //20
		{
			leftBoudary = false;
			rightBoundary = true;
		}
		else
			shoulder += speed; // 0.025f;
			
	}

	if (rightBoundary)
	{
		
		if (shoulder < -angle)
		{
			leftBoudary = true;
			rightBoundary = false;
		}
		shoulder -= speed;
	}
	//  fprintf(gpFILE, "shoulder %f  speed %f angle %f\n", shoulder, speed, angle);

}
void updateElbow()
{
	
}

void updateThungForSitting()
{
	thung = 40.0f;
}

void updateKneeForSitting()
{
	knee = -80;
}
void udpateThung(GLfloat speed, GLfloat angle)
{
	static bool leftBoudary = false;
	static bool rightBoundary = true;

	if (leftBoudary)
	{
		thung += speed;//0.01f
		if (thung >= angle)
		{
			leftBoudary = false;
			rightBoundary = true;
		}

	}

	if (rightBoundary)
	{
		thung -= speed;
		if (thung < -angle)
		{
			leftBoudary = true;
			rightBoundary = false;
		}
	}
	//fprintf(gpFILE, "shoulder %d\n", shoulder);

}
void udpateKnee()
{

}

void DrawCube()
{
	GLfloat CubeSideLength = 1.5f;
	GLfloat CubeSide = CubeSideLength / 2.0f;
	glBegin(GL_QUADS);
	//front
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(CubeSide, CubeSide, CubeSide);
	glVertex3f(-CubeSide, CubeSide, CubeSide);
	glVertex3f(-CubeSide, -CubeSide, CubeSide);
	glVertex3f(CubeSide, -CubeSide, CubeSide);

	//right
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(CubeSide, CubeSide, -CubeSide);
	glVertex3f(CubeSide, CubeSide, CubeSide);
	glVertex3f(CubeSide, -CubeSide, CubeSide);
	glVertex3f(CubeSide, -CubeSide, -CubeSide);

	//back
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-CubeSide, CubeSide, -CubeSide);
	glVertex3f(CubeSide, CubeSide, -CubeSide);
	glVertex3f(CubeSide, -CubeSide, -CubeSide);
	glVertex3f(-CubeSide, -CubeSide, -CubeSide);

	//left
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-CubeSide, CubeSide, CubeSide);
	glVertex3f(-CubeSide, CubeSide, -CubeSide);
	glVertex3f(-CubeSide, -CubeSide, -CubeSide);
	glVertex3f(-CubeSide, -CubeSide, CubeSide);

	//top - spider man.. facing screen and backside at you
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(CubeSide, CubeSide, -CubeSide);
	glVertex3f(-CubeSide, CubeSide, -CubeSide);
	glVertex3f(-CubeSide, CubeSide, CubeSide);
	glVertex3f(CubeSide, CubeSide, CubeSide);

	//top - spider man.. breaks the top of cube and fall as it is. backside at you and facing screen
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(CubeSide, -CubeSide, -CubeSide);
	glVertex3f(-CubeSide, -CubeSide, -CubeSide);
	glVertex3f(-CubeSide, -CubeSide, CubeSide);
	glVertex3f(CubeSide, -CubeSide, CubeSide);


	glEnd();

}

void LeftHandMovement(GLfloat leftShoulder, GLfloat leftElbow)
{
	glPushMatrix(); //ithe parat yaycha ahe mhanun push
	glRotatef((GLfloat)leftShoulder, 1.0f, 0.0f, 0.0f);
	glTranslatef(-0.95f, -1.2f, 0.0f);
	glPushMatrix();
	glColor3f(0.5f, 0.35, 0.05f);
	glScalef(0.8f, 2.0f, 1.0f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.25f, 10, 10);
	glPopMatrix();


	glTranslatef(0.0f, -0.4f, 0.0f);
	glRotatef((GLfloat)leftElbow, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glScalef(0.7f, 2.0f, 0.8f);
	glColor3f(0.5f, 0.35, 0.05f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.25, 10, 10);
	glPopMatrix();

	glPopMatrix();
}

void RightHandMovement(GLfloat rightShoulder, GLfloat rightElbow)
{
	glPushMatrix(); //ithe parat yaycha ahe mhanun push
	glRotatef(-(GLfloat)rightShoulder, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.95f, -1.2f, 0.0f);
	glPushMatrix();
	glScalef(0.8f, 2.0f, 1.0f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.25f, 10, 10);
	glPopMatrix();


	glTranslatef(0.0f, -0.4f, 0.0f);
	glRotatef((GLfloat)rightElbow, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glScalef(0.6f, 2.0f, 0.8f);
	glColor3f(0.5f, 0.35, 0.05f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.25, 10, 10);
	glPopMatrix();

	glPopMatrix();
}

void LeftLegMovement(GLfloat leftThung, GLfloat leftKnee)
{
	glPushMatrix(); //ithe parat yaycha ahe mhanun push
	glRotatef(-(GLfloat)leftThung, 1.0f, 0.0f, 0.0f);
	glTranslatef(-0.37f, -2.25f, 0.0f); //-2.25
	glPushMatrix();
	glScalef(0.6f, 2.0f, 0.6f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5f, 10, 10);
	glPopMatrix();


	glTranslatef(0.0f, -1.0f, 0.0f);
	glRotatef((GLfloat)leftKnee, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glScalef(0.6f, 2.0f, 0.6f);
	glColor3f(0.5f, 0.35, 0.05f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.4f, 10, 10);
	glPopMatrix();

	//toe
	glTranslatef(0.0f, -0.8f, -0.2f);
	glColor3f(0.5f, 0.35, 0.05f);
	glScalef(1.0f, 1.0f, 1.3f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 10, 10);

	glPopMatrix();

}

void RightLegMovement(GLfloat rightThung, GLfloat rightKnee)
{
	glPushMatrix(); //ithe parat yaycha ahe mhanun push
	glRotatef((GLfloat)rightThung, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.37f, -2.25f, 0.0f);
	glPushMatrix();
	glScalef(0.6f, 2.0f, 0.6f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.5f, 10, 10);
	glPopMatrix();


	glTranslatef(0.0f, -1.0f, 0.0f);
	glRotatef((GLfloat)rightKnee, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glScalef(0.6f, 2.0f, 0.6f);
	glColor3f(0.5f, 0.35, 0.05f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.4f, 10, 10);
	glPopMatrix();

	//toe
	glTranslatef(0.0f, -0.8f, -0.2f);
	glColor3f(0.5f, 0.35, 0.05f);
	glScalef(1.0f, 1.0f, 1.3f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 10, 10);

	glPopMatrix();
}

void DrawHumanoid()
{
	glPushMatrix();
	//glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	glScalef(1.0f, 1.2f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);//GL_POINT GL_FILL
	glColor3f(1.0f, 1.0f, 0.0f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.40f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -0.4f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	quadric = NULL;
	quadric = gluNewQuadric();
	gluCylinder(quadric, 0.2f, 0.2f, 0.3f, 10, 10);
	glPopMatrix();

	//pot
	glPushMatrix();
	glTranslatef(0.0f, -1.5f, 0.0f);
	//DrawCube();
	glColor3f(0.0f, 1.0f, 1.0f);
	glScalef(1.0f, 1.3f, 0.8f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75f, 50, 50);
	
	glPopMatrix();

	//update shoulder movment
	
	LeftHandMovement(shoulder, elbow);
	//right hand
	RightHandMovement(shoulder, elbow);

	//left Leg
	LeftLegMovement(thung, knee);

	//right Leg
	RightLegMovement(thung, knee);

}

void running()
{
	updateShoulder(0.050f, 20.0f);
	udpateThung(0.02, 8.0f);
	DrawHumanoid();
}


void walking()
{
	static GLfloat rightShoulder = 0.0f;
	static GLfloat rightElbow = 0.0f;
	static GLfloat leftShoulder = 0.0f;
	static GLfloat leftElbow = 0.0f;

	static GLfloat rightThung = 0.0f;
	static GLfloat rightKnee = 0.0f;
	static GLfloat leftThung = 0.0f;
	static GLfloat leftKnee = 0.0f;

	updateShoulder(0.025f, 20.0f);
	udpateThung(0.01f, 8.0f);
/*
	updateRightShoulder(rightShoulder);
	updateRightElbow(rightElbow);

	updateLeftShoulder();
	updateLeftElbow();

	updateLeftThung();
	updateLeftKnee();
	
	updateRightThung();
	updateRightKnee();
*/
	DrawHumanoid();
}

void sit()
{
	/*
	rightThung = 20;
	rightKnee = 104;
	leftThung = -20;
	leftKnee = 104;
	//updateThungForSitting();
	//updateKneeForSitting();
	DrawHumanoid();
	*/
}