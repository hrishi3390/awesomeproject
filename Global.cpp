#include "Global.h"

//texture variables
GLuint wall_texture;
GLuint kundali_texture;


 GLuint tile_blue;
 GLuint tile_cyan;
 GLuint tile_green;
 GLuint tile_orange;
 GLuint tile_purple;
 GLuint tile_red;
 GLuint tile_yellow;

GLfloat Angle = 0.0f;


bool LoadGLTexture(GLuint *texture, TCHAR resouceID[])
{
	//variable declaration
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resouceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION); //Load Resource create DIB section, device independent bitmat

	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);// get information from OS
												 //actual texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture); //like calloc
												//setting of texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//MAGNIFICATION
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//following call willactually push data to grahics memorny with help of grahics driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);// GL_BGT_TXT changes with image type
																												 //in programmable pipeline 1) glubuild2dmipmaps = glteximage glGeneratemimap
		DeleteObject(hBitmap);
	}

	return bResult;
}

void LoadAndEnableTexture()
{

	LoadGLTexture(&kundali_texture, MAKEINTRESOURCE(IDB_BITMAP1));
	LoadGLTexture(&wall_texture, MAKEINTRESOURCE(WALL_TEXTURE));

	//tile colors
	LoadGLTexture(&tile_blue, MAKEINTRESOURCE(IDB_BITMAP2));
	LoadGLTexture(&tile_cyan, MAKEINTRESOURCE(IDB_BITMAP3));
	LoadGLTexture(&tile_green, MAKEINTRESOURCE(IDB_BITMAP4));
	LoadGLTexture(&tile_orange, MAKEINTRESOURCE(IDB_BITMAP5));
	LoadGLTexture(&tile_purple, MAKEINTRESOURCE(IDB_BITMAP6));
	LoadGLTexture(&tile_red, MAKEINTRESOURCE(IDB_BITMAP7));
	LoadGLTexture(&tile_yellow, MAKEINTRESOURCE(IDB_BITMAP8));
	

	glEnable(GL_TEXTURE_2D);

}

void updateAngle()
{
	 Angle = Angle + 0.08f;//3
	if (Angle >= 90.0f)
	{
		Angle = 0.0f;
	}
}