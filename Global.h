#pragma once
#include<Windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/glu.h>
#define _USE_MATH_DEFINES
#include <math.h>

#include "resource.h"

//texture variable
extern GLuint wall_texture;
extern GLuint kundali_texture;

extern GLuint tile_blue;
extern GLuint tile_cyan;
extern GLuint tile_green;
extern GLuint tile_orange;
extern GLuint tile_purple;
extern GLuint tile_red;
extern GLuint tile_yellow;


extern GLfloat Angle;
extern bool MoveLeft;
extern bool MoveRight;
extern bool RotateClock;
extern bool RotateAntiClock;

extern FILE* gpFILE ;
//extern bool MoveCount;



void LoadAndEnableTexture();
void updateAngle();