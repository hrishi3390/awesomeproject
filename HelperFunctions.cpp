
#include "HelperFunctions.h"
#include "Global.h"


 bool MoveLeft =false;
 bool MoveRight = false;
 bool RotateClock = false;
 bool RotateAntiClock = false;

void DrawSquare (GLuint texture)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	//cube
	glBegin(GL_QUADS);
	//front
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();



}

void DrawLineShape()
{
	DrawSquare(tile_cyan);
	glTranslatef(1.0f, 0.0f,0.0f);
	DrawSquare(tile_cyan);
	glTranslatef(1.0f, 0.0f, 0.0f);
	DrawSquare(tile_cyan);
	glTranslatef(1.0f, 0.0f, 0.0f);
	DrawSquare(tile_cyan);
}

void DrawTShape()
{
	DrawSquare(tile_purple);
	glTranslatef(1.0f, 0.0f, 0.0f);
	DrawSquare(tile_purple);
	glTranslatef(0.0f, 1.0f, 0.0f);
	DrawSquare(tile_purple);
	glTranslatef(1.0f, -1.0f, 0.0f);
	DrawSquare(tile_purple);

}

void DrawBoxShape()
{
	DrawSquare(tile_yellow);
	glTranslatef(0.0f, -1.0f, 0.0f);
	DrawSquare(tile_yellow);
	glTranslatef(1.0f, 1.0f, 0.0f);
	DrawSquare(tile_yellow);
	glTranslatef(0.0f, -1.0f, 0.0f);
	DrawSquare(tile_yellow);
}

void DrawLShape()
{
	DrawSquare(tile_blue);
	glTranslatef(1.0f, 0.0f, 0.0f);
	DrawSquare(tile_blue);
	glTranslatef(0.0f, 1.0f, 0.0f);
	DrawSquare(tile_blue);
	glTranslatef(0.0f, 1.0f, 0.0f);
	DrawSquare(tile_blue);
}

void DrawNShape()
{
	DrawSquare(tile_red);
	glTranslatef(0.0f, 1.0f, 0.0f);
	DrawSquare(tile_red);
	glTranslatef(1.0f, 0.0f, 0.0f);
	DrawSquare(tile_red);
	glTranslatef(0.0f, 1.0f, 0.0f);
	DrawSquare(tile_red);
}




void DrawGraph(GLfloat monitorWidth, GLfloat monitorHeight, GLfloat distanceBetweenLines, GLfloat lineWidth)
{
	glLineWidth(lineWidth);
	glBegin(GL_LINES);

	GLfloat xCordinate = distanceBetweenLines;
	GLfloat yCoordiante = monitorHeight / 2;

	int i = 1;
	while (xCordinate < monitorWidth / 2)
	{
		xCordinate = i*distanceBetweenLines;

		glColor3f(0.0f, 0.0f, 1.0f);

		glVertex3f(xCordinate, yCoordiante, 0.0f);
		glVertex3f(xCordinate, -yCoordiante, 0.0f);

		glVertex3f(-xCordinate, yCoordiante, 0.0f);
		glVertex3f(-xCordinate, -yCoordiante, 0.0f);
		i++;
	}
	glEnd();

	glBegin(GL_LINES);

	xCordinate = monitorWidth / 2;
	yCoordiante = distanceBetweenLines;

	i = 1;
	while (yCoordiante < monitorHeight / 2)
	{
		yCoordiante = i*distanceBetweenLines;

		glColor3f(0.0f, 0.0f, 1.0f);

		glVertex3f(-xCordinate, yCoordiante, 0.0f);
		glVertex3f(xCordinate, yCoordiante, 0.0f);

		glVertex3f(-xCordinate, -yCoordiante, 0.0f);
		glVertex3f(xCordinate, -yCoordiante, 0.0f);
		i++;
	}

	glEnd();



	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, monitorHeight / 2, 0.0f);
	glVertex3f(0.0f, -monitorHeight / 2, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(monitorWidth / 2, 0, 0.0f);
	glVertex3f(-monitorWidth / 2, 0, 0.0f);
	glEnd();


}


void MoveObjectToLeft()
{
	MoveLeft = true;

}
void MoveObjectToRight()
{
	MoveRight = true;
}
void RotateClockWise()
{
	RotateClock = true;
	
}
void RotateAntiClockWise()
{
	RotateAntiClock = true;
}


//static GLfloat translate = 10.0f;
//static GLfloat translateTowarRightWall = 10.0f;
//const GLfloat speedLimit = 0.018;

//Movement related functions


bool ToAndFroMovement(GLfloat speedLimit, GLfloat leftMovement, GLfloat rightMovement, void (*fun)())
{
	int static count = 4;

	if (count)
	{
		static GLfloat translate = rightMovement;
		static GLfloat translateTowarRightWall = 10.0f;
		//const GLfloat speedLimit = 0.006;
		static bool endOfleftWall = false;
		static bool endOfRightWall = true;
		glPushMatrix();

		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		//walk towards the wall


		if (endOfRightWall)

			if (translate >= leftMovement) //-10.0f
			{
				translate -= speedLimit;
				glTranslatef(0.0f, 0.0f, translate);
				fun();
			}
			else
			{
				endOfleftWall = true;
				endOfRightWall = false;
				translateTowarRightWall = 10.0f;
				count--;
				//glRotated(180.0f, 0.0f, 1.0f, 0.0f);
			}


		glPopMatrix();
		glPushMatrix();

		//translate = 0.0f;

		if (endOfleftWall == true)
		{


			if (translateTowarRightWall >= -rightMovement)
			{
				glRotated(-90.0f, 0.0f, 1.0f, 0.0f);
				translateTowarRightWall -= speedLimit;
				glTranslatef(0.0f, 0.0f, translateTowarRightWall);
				fun();
			}
			else
			{
				endOfRightWall = true;
				endOfleftWall = false;
				translate = 10.0f;
				count--;
			}

		}
		//walking();
		glPopMatrix();
		//count--;
	}
	else
		return true;

	return false;
}

