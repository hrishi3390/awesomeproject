#include<Windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/glu.h> //for perpective
#include "Global.h"
#include <mmsystem.h>

//user defined header files
#include "Global.h"
#include "Scene.h"
#include "HelperFunctions.h"
#include "Humanoid.h"

#pragma comment(lib,  "winmm.lib")

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gpFullscreen = false;
HWND ghwnd = NULL;
FILE* gpFILE = NULL;
bool gpActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

static GLfloat MoveLeftCount;
static GLfloat MoveRightCount;
static GLfloat RotateCount;
static GLfloat RotateAntiCount;
static GLfloat MoveCount;
//CALLBACK is calling convention with typedef+far_Pascal CALLBACK
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//HINSTANCE Operating system uses this value to identify executable when loaded
//HPrevInstance handle to prev instance of same program sharing same text region
//lpszcmdline : command line arguments
//icmdshow: show whether main application window will be maximize, min or show normally
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstace, LPSTR lpszCmdLine, int iCmdShow)
{
	//variables
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SolarSystem");
	bool bDone = false;

	//prototype
	void Initialize(void);
	void Display();

	if (fopen_s(&gpFILE, "gui.log", "w") != 0)
	{
		MessageBox(NULL, TEXT("Unable to open a file"), TEXT("Error"), MB_OK);
		exit(0);
	}

	//cbsize: count of bytes
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	RECT rect;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
	int iWidthScreen = rect.right - rect.left;
	int iHeightScreen = rect.bottom - rect.top;

	int iXCenter = (iWidthScreen / 2) - (WIN_WIDTH / 2);
	int iYCenter = (iHeightScreen / 2) - (WIN_HEIGHT / 2);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SolarSystem"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iXCenter,
		iYCenter,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//PlaySound((LPCTSTR)IDR_WAVE1, NULL, SND_RESOURCE | SND_ASYNC);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gpActiveWindow == true)
			{
				Display();			
				updateAngle();
			}
		}
	}
	return(int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local var
	void Resize(int, int);
	void Unitialize();
	void ToggleFullscreen(void);

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
		case 0x66:
			ToggleFullscreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case VK_LEFT:
			MoveObjectToLeft();
			break;
		case VK_RIGHT:
			MoveObjectToRight();
			break;
		case VK_UP:
			RotateClockWise();
			break;
		case VK_DOWN:
			RotateAntiClockWise();
			break;
		

		default:
			break;
		}


	case WM_SETFOCUS:
		gpActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gpActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_DESTROY:
		Unitialize();
		PostQuitMessage(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//If you have changed certain window data using SetWindowLong, you must call SetWindowPos for the changes to take effect
	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gpFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~(WS_OVERLAPPEDWINDOW)));
				int x = mi.rcMonitor.right - mi.rcMonitor.left;
				int y = mi.rcMonitor.bottom - mi.rcMonitor.top;
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, x, y, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gpFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
		gpFullscreen = false;
	}
}

void Initialize()
{
	void Resize(int, int);
	bool LoadGLTexture(GLuint*, TCHAR[]);
	//var
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	pfd.cAlphaBits = 32;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFILE, "Error in pized format index\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		fprintf(gpFILE, "Error in setPixedFormat()\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		fprintf(gpFILE, "Error in wglCreateContext\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "Error in wglCreateContext()\n");
		DestroyWindow(ghwnd);
	}

	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	//for texture loading texture
	LoadAndEnableTexture();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Resize(WIN_WIDTH, WIN_HEIGHT);

}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 15.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//glTranslatef(0.0f, 5.0f, 0.0f);
	//DrawWorld();
	
	GLfloat speedOfMovement = 0.006f;
	GLfloat leftMovement = -10.0f;
	GLfloat rightMovement = 10.0f;

	//Humonoid walking to and fro
	bool isToAndFroMovementDone = ToAndFroMovement(speedOfMovement, leftMovement, rightMovement, walking);
	//bool isToAndFroMovementDone = true;
	if (isToAndFroMovementDone)
	{
		//glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		//glTranslatef(10.0f, 0.0f, 0.0f);
		//sit();
	}
	

	
	SwapBuffers(ghdc);
}

void Unitialize()
{
	if (gpFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | (WS_OVERLAPPEDWINDOW)));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFILE)
	{
		fclose(gpFILE);
		gpFILE = NULL;
	}

}

void Resize(int WM_WIDTH, int WM_HEIGHT)
{
	if (WM_HEIGHT == 0)
		WM_HEIGHT = 1;
	glViewport(0, 0, (GLsizei)WM_WIDTH, (GLsizei)WM_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();//you are at center
	gluPerspective(45.0f, (GLfloat)WM_WIDTH / (GLfloat)WM_HEIGHT, 0.1f, 100.0f);// this is my frustum

}