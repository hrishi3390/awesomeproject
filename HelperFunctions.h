#pragma once
#include<Windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/glu.h> //for perpective
#include<math.h>s



void DrawLineShape();
void DrawTShape();
void DrawBoxShape();
void DrawLShape();
void DrawNShape();
void DrawGraph(GLfloat monitorWidth, GLfloat monitorHeight, GLfloat distanceBetweenLines, GLfloat lineWidth);


void MoveObjectToLeft();
void MoveObjectToRight();
void RotateClockWise();
void RotateAntiClockWise();



//Movements
bool ToAndFroMovement(GLfloat speedOfMovement, GLfloat leftMovement, GLfloat rightMovement, void (*fun)());