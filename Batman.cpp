
#include "batman.h"


void DrawBatman()
{
	glBegin(GL_POLYGON);
	glVertex3d(0, 1.4, 0);
	glVertex3d(.1, 1.37, 0);
	glVertex3d(.2, 1.35, 0);
	glVertex3d(.2, 0, 0);
	glVertex3d(0, 0, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(.2, 1.35, 0);
	glVertex3d(.4, 2, 0);
	glVertex3d(.5, 1, 0);
	glVertex3d(.5, -1, 0);
	glVertex3d(.2, 0, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(0, 0, 0);
	glVertex3d(.6, 0, 0);
	glVertex3d(.5, -1, 0);
	glVertex3d(0, -2, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(0.3, 0.9, 0);
	glVertex3d(0.8, 0.9, 0);
	glVertex3d(1.1, -0.6, 0);
	glVertex3d(0.5, -1, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(0, 0, 0);
	glVertex3d(0.85, -.75, 0);
	glVertex3d(0.4, -1.15, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(0.8, 0.9, 0);
	glVertex3d(1.2, 1, 0);
	glVertex3d(2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(0.8, 0.9, 0);
	glVertex3d(1.1, -0.6, 0);
	glVertex3d(2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(1.2, 1, 0);
	glVertex3d(1.4, 1.1, 0);
	glVertex3d(3.9, 0, 0);
	glVertex3d(2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(1.4, 1.1, 0);
	glVertex3d(1.6, 1.3, 0);
	glVertex3d(4.15, 0.5, 0);
	glVertex3d(3.9, 0, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(1.6, 1.3, 0);
	glVertex3d(1.75, 1.6, 0);
	glVertex3d(4.4, 0.85, 0);
	glVertex3d(4.15, 0.5, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(1.75, 1.6, 0);
	glVertex3d(1.85, 1.9, 0);
	glVertex3d(5, 1.3, 0);
	glVertex3d(4.4, 0.85, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(1.85, 1.9, 0);
	glVertex3d(1.9, 2.2, 0);
	glVertex3d(7, 2.2, 0);
	glVertex3d(5, 1.3, 0);
	glEnd();


	//left side

	glBegin(GL_POLYGON);
	glVertex3d(0, 1.4, 0);
	glVertex3d(-0.1, 1.37, 0);
	glVertex3d(-0.2, 1.35, 0);
	glVertex3d(-0.2, 0, 0);
	glVertex3d(0, 0, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(-0.2, 1.35, 0);
	glVertex3d(-0.4, 2, 0);
	glVertex3d(-0.5, 1, 0);
	glVertex3d(-0.5, -1, 0);
	glVertex3d(-0.2, 0, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(0, 0, 0);
	glVertex3d(-0.6, 0, 0);
	glVertex3d(-0.5, -1, 0);
	glVertex3d(0, -2, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(-0.3, 0.9, 0);
	glVertex3d(-0.8, 0.9, 0);
	glVertex3d(-1.1, -0.6, 0);
	glVertex3d(-0.5, -1, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(0, 0, 0);
	glVertex3d(-0.85, -.75, 0);
	glVertex3d(-0.4, -1.15, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(-0.8, 0.9, 0);
	glVertex3d(-1.2, 1, 0);
	glVertex3d(-2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(-0.8, 0.9, 0);
	glVertex3d(-1.1, -0.6, 0);
	glVertex3d(-2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(-1.2, 1, 0);
	glVertex3d(-1.4, 1.1, 0);
	glVertex3d(-3.9, 0, 0);
	glVertex3d(-2.35, -0.1, 0);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3d(-1.4, 1.1, 0);
	glVertex3d(-1.6, 1.3, 0);
	glVertex3d(-4.15, 0.5, 0);
	glVertex3d(-3.9, 0, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(-1.6, 1.3, 0);
	glVertex3d(-1.75, 1.6, 0);
	glVertex3d(-4.4, 0.85, 0);
	glVertex3d(-4.15, 0.5, 0);
	glEnd();


	glBegin(GL_POLYGON);
	glVertex3d(-1.75, 1.6, 0);
	glVertex3d(-1.85, 1.9, 0);
	glVertex3d(-5, 1.3, 0);
	glVertex3d(-4.4, 0.85, 0);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3d(-1.85, 1.9, 0);
	glVertex3d(-1.9, 2.2, 0);
	glVertex3d(-7, 2.2, 0);
	glVertex3d(-5, 1.3, 0);
	glEnd();


}


void FlyingBats()
{
	glTranslatef(0.0f, 0.0f, -100.0f);

	static bool flag = false;

	if (!flag)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		flag = true;
	}
	else
	{
		glTranslatef(0.0f, 0.0f, 0.0f);
		flag = false;
	}


	for (int i = 0; i <= 20; i++)
	{
		glTranslatef(-15.0f, 0.0f, 0.0f);
		DrawBatman();
	}


}